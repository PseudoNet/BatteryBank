# 18650 Battery Bank
PCB design for an 18650 batery bank using standard components:
- [BH-18650-PCB Battery holder](http://s.click.aliexpress.com/e/c6bvVavK)
- [BLX-A Fuse Holders](http://s.click.aliexpress.com/e/xPDgUv6) or [Generic 20*5 fuse holder](http://s.click.aliexpress.com/e/kGASTZm)
- [5x20mm glass fuse (I use 1A)](http://s.click.aliexpress.com/e/by5PpD7a)
- [KF1000 2P terminal](http://s.click.aliexpress.com/e/8iBWgIc)
## Instructions
- Download [BatteryBank2.zip](/BatteryBank2.zip)
- Upload and purchase fron your preferred PCB manufacturer (i.e. [JLCPCB](https://jlcpcb.com/))
- Solder in components
## Images
![PCB](/images/BatteryBank.png)
![Photo01](/images/Photo01.jpg)
![Photo02](/images/Photo02.jpg)
![Photo03](/images/Photo03.jpg)